Pod::Spec.new do |s|
  s.name             = 'Transactions'
  s.version          = '18.0.4'
  s.summary          = 'Transactions SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-transactions.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "TransactionSDK.xcframework"
  
  s.dependency 'SSZipArchive', '~> 2.4'
  s.swift_versions = '5.0'
end