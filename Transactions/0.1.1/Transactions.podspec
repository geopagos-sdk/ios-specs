Pod::Spec.new do |s|
  s.name             = 'Transactions'
  s.version          = '0.1.1'
  s.summary          = 'Transactions SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-transactions.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.public_header_files = "TransactionSDK.framework/Headers/*.h"
  s.source_files = "TransactionSDK.framework/Headers/*.h"
  s.vendored_frameworks = "TransactionSDK.framework"
  
  s.dependency 'Alamofire', '~> 5.2'
  s.swift_versions = '5.0'
end
