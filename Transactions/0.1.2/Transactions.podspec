Pod::Spec.new do |s|
  s.name             = 'Transactions'
  s.version          = '0.1.2'
  s.summary          = 'Transactions SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-transactions.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.public_header_files = "TransactionSDK.framework/Headers/*.h"
  s.source_files = "TransactionSDK.framework/Headers/*.h"
  s.vendored_frameworks = "TransactionSDK.framework"
  
  s.dependency 'Alamofire', '~> 5.2'
  s.swift_versions = '5.0'

  s.script_phases = [
    { :name => 'MagicPosSDK', :script => 'if [ -d "${BUILT_PRODUCTS_DIR}/MagicPosFramework" ]; then
echo "${BUILT_PRODUCTS_DIR}/MagicPosFramework directory already exists, so skipping the rest of the script."
exit 0
fi

mkdir -p "${BUILT_PRODUCTS_DIR}/MagicPosFramework"
cat <<EOF > "${BUILT_PRODUCTS_DIR}/MagicPosFramework/module.modulemap"
module MagicPosSDK {
    header "${PODS_ROOT}/Transactions/TransactionSDK.framework/Headers/MPOSService.h"
    header "${PODS_ROOT}/Transactions/TransactionSDK.framework/Headers/BTDeviceScanner.h"
    header "${PODS_ROOT}/Transactions/TransactionSDK.framework/Headers/Utils.h"
    export *
}
EOF', :execution_position => :before_compile , :shell_path => '/bin/sh' },
{ :name => 'QPosPosSDK', :script => 'if [ -d "${BUILT_PRODUCTS_DIR}/QPosFramework" ]; then
echo "${BUILT_PRODUCTS_DIR}/QPosFramework directory already exists, so skipping the rest of the script."
exit 0
fi

mkdir -p "${BUILT_PRODUCTS_DIR}/QPosFramework"
cat <<EOF > "${BUILT_PRODUCTS_DIR}/QPosFramework/module.modulemap"
module QPosSDK {
    header "${PODS_ROOT}/Transactions/TransactionSDK.framework/Headers/QPOSService.h"
    header "${PODS_ROOT}/Transactions/TransactionSDK.framework/Headers/BTDeviceFinder.h"
    header "${PODS_ROOT}/Transactions/TransactionSDK.framework/Headers/QPOSUtil.h"
    header "${PODS_ROOT}/Transactions/TransactionSDK.framework/Headers/EMVTLV.h"
    export *
}
EOF', :execution_position => :before_compile , :shell_path => '/bin/sh' }
]
end
