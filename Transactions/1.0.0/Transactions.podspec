Pod::Spec.new do |s|
  s.name             = 'Transactions'
  s.version          = '1.0.0'
  s.summary          = 'Transactions SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-transactions.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.public_header_files = "TransactionSDK.framework/Headers/*.h"
  s.source_files = "TransactionSDK.framework/Headers/*.h"
  s.vendored_frameworks = "TransactionSDK.framework"
  
  s.dependency 'Alamofire', '~> 5.2'
  s.swift_versions = '5.0'

  s.script_phases = [
    { :name => 'MagicPosSDK', :script => 'if [ -d "${PODS_CONFIGURATION_BUILD_DIR}/MagicPosFramework" ]; then
echo "${PODS_CONFIGURATION_BUILD_DIR}/MagicPosFramework directory already exists, so skipping the rest of the script."
exit 0
fi

mkdir -p "${PODS_CONFIGURATION_BUILD_DIR}/MagicPosFramework"
cat <<EOF > "${PODS_CONFIGURATION_BUILD_DIR}/MagicPosFramework/module.modulemap"
module MagicPosSDK {
    header "${PODS_TARGET_SRCROOT}/TransactionSDK.framework/Headers/MPOSService.h"
    header "${PODS_TARGET_SRCROOT}/TransactionSDK.framework/Headers/BTDeviceScanner.h"
    header "${PODS_TARGET_SRCROOT}/TransactionSDK.framework/Headers/Utils.h"
    export *
}
EOF', :execution_position => :before_compile , :shell_path => '/bin/sh' },
{ :name => 'QPosPosSDK', :script => 'if [ -d "${PODS_CONFIGURATION_BUILD_DIR}/QPosFramework" ]; then
echo "${PODS_CONFIGURATION_BUILD_DIR}/QPosFramework directory already exists, so skipping the rest of the script."
exit 0
fi

mkdir -p "${PODS_CONFIGURATION_BUILD_DIR}/QPosFramework"
cat <<EOF > "${PODS_CONFIGURATION_BUILD_DIR}/QPosFramework/module.modulemap"
module QPosSDK {
    header "${PODS_TARGET_SRCROOT}/TransactionSDK.framework/Headers/QPOSService.h"
    header "${PODS_TARGET_SRCROOT}/TransactionSDK.framework/Headers/BTDeviceFinder.h"
    header "${PODS_TARGET_SRCROOT}/TransactionSDK.framework/Headers/QPOSUtil.h"
    header "${PODS_TARGET_SRCROOT}/TransactionSDK.framework/Headers/EMVTLV.h"
    export *
}
EOF', :execution_position => :before_compile , :shell_path => '/bin/sh' }
]
end
