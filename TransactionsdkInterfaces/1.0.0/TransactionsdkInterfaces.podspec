Pod::Spec.new do |s|
  s.name             = 'TransactionsdkInterfaces'
  s.version          = '1.0.0'
  s.summary          = 'Interfaces to conform to when adding plugins'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-transactionsdkpluginsinterfaces.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "Transactionsdk_plugins_interfaces.xcframework"
  
  s.swift_versions = '5.0'
end