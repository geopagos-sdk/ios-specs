Pod::Spec.new do |s|
  s.name             = 'MagicPosHardware'
  s.version          = '9.0.3'
  s.summary          = 'MagicPosHardware SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-magicpossdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "MagicPosHardware.xcframework"
  
  s.dependency 'Transactions', '~> 12.0.0'
  s.swift_versions = '5.0'

  # Skip this architecture to pass Pod validation since we removed the `arm64` simulator ARCH in order to use lipo later
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
