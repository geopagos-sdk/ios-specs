Pod::Spec.new do |s|
  s.name             = 'MagicPosHardware'
  s.version          = '6.0.3'
  s.summary          = 'MagicPosHardware SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-magicpossdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.public_header_files = "MagicPosHardware.framework/Headers/*.h"
  s.source_files = "MagicPosHardware.framework/Headers/*.h"
  s.vendored_frameworks = "MagicPosHardware.framework"
  
  s.dependency 'Transactions', '~> 10.0.1'
  s.swift_versions = '5.0'
end
