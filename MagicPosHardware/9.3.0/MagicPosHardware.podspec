Pod::Spec.new do |s|
  s.name             = 'MagicPosHardware'
  s.version          = '9.3.0'
  s.summary          = 'MagicPosHardware SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-magicpossdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "MagicPosHardware.xcframework"
  
  s.dependency 'Transactions', '~> 16.0'
  s.swift_versions = '5.0'
end
