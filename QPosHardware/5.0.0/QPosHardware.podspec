Pod::Spec.new do |s|
  s.name             = 'QPosHardware'
  s.version          = '5.0.0'
  s.summary          = 'QPosHardware SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-qpossdk.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.public_header_files = "QPosHardware.framework/Headers/*.h"
  s.source_files = "QPosHardware.framework/Headers/*.h"
  s.vendored_frameworks = "QPosHardware.framework"
  
  s.dependency 'Transactions', '~> 8.0.0'
  s.swift_versions = '5.0'
end
