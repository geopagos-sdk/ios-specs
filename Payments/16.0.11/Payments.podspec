Pod::Spec.new do |s|
  s.name             = 'Payments'
  s.version          = '16.0.11'
  s.summary          = 'Payments SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-payments.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "Payments.xcframework"
  
  s.dependency 'Transactions', '~> 18.0', '< 18.1'
  s.dependency 'GeoURLSessionHTTPClient', '~> 2.0'
  s.dependency 'TransactionsdkInterfaces', '~> 1.3'  

  s.swift_versions = '5.0'
end
