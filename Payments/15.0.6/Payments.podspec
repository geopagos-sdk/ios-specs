Pod::Spec.new do |s|
  s.name             = 'Payments'
  s.version          = '15.0.6'
  s.summary          = 'Payments SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-payments.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "Payments.xcframework"
  

  s.dependency 'Transactions', '~> 16.0'
  s.dependency 'AFNetworkingHTTPClient', '~> 2.0.0'  
  s.swift_versions = '5.0'
end
