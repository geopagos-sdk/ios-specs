Pod::Spec.new do |s|
  s.name             = 'Payments'
  s.version          = '13.0.4'
  s.summary          = 'Payments SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-payments.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "Payments.xcframework"
  

  s.dependency 'Transactions', '~> 13.0'
  s.dependency 'AFNetworkingHTTPClient', '~> 2.0.0'

  
  s.swift_versions = '5.0'

    # Skip this architecture to pass Pod validation since we removed the `arm64` simulator ARCH in order to use lipo later
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
