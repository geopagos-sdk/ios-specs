Pod::Spec.new do |s|
  s.name             = 'Payments'
  s.version          = '0.0.2'
  s.summary          = 'Payments SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-payments.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.public_header_files = "Payments.framework/Headers/*.h"
  s.source_files = "Payments.framework/Headers/*.h"
  s.vendored_frameworks = "Payments.framework"
  
  s.dependency 'Transactions', '~> 1.1.0-alpha1'
  s.dependency 'QPosHardware', '~> 0.0.1'
  s.dependency 'MagicPosHardware', '~> 0.0.1'
  s.swift_versions = '5.0'
end
