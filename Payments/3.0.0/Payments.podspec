Pod::Spec.new do |s|
  s.name             = 'Payments'
  s.version          = '3.0.0'
  s.summary          = 'Payments SDK'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-payments.git', :tag => s.version.to_s }

  s.ios.deployment_target = '10.0'

  s.public_header_files = "Payments.framework/Headers/*.h"
  s.source_files = "Payments.framework/Headers/*.h"
  s.vendored_frameworks = "Payments.framework"
  
  s.dependency 'Transactions', '~> 2.0.1'
  s.dependency 'QPosHardware', '~> 2.0.0'
  s.dependency 'MagicPosHardware', '~> 2.0.0'
  s.swift_versions = '5.0'
end
