Pod::Spec.new do |s|
  s.name             = 'GeoSignaturePlugin'
  s.version          = '1.0.6'
  s.summary          = 'Allows to request an on-screen signature'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-signature.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "Signature.xcframework"
  
  s.dependency 'Transactions', '~> 16.0'
  s.swift_versions = '5.0'
end