Pod::Spec.new do |s|
  s.name             = 'GePanHashPlugin'
  s.version          = '1.0.1'
  s.summary          = 'Requests a hash of the credit card PAN'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-panhashplugin.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'

  s.vendored_frameworks = "GeoiOSpanHashPlugin.xcframework"
  
  s.dependency 'TransactionsdkInterfaces', '~> 1.0'
  s.dependency 'GeoURLSessionHTTPClient', '~> 2.0'

  s.swift_versions = '5.0'
end