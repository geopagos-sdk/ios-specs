#
#  Be sure to run `pod spec lint AFNetworkingHTTPClient.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "AFNetworkingHTTPClient"
  spec.version      = "2.1.0"
  spec.summary      = "AFNetworkingHTTPClient SDK"

  spec.license          = { :type => 'Copyright' }
  spec.author           = 'Geopagos'
  spec.ios.deployment_target = '13.0'
  spec.homepage         = 'https://www.geopagos.com'
  spec.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-afnetworkinghttpclient.git', :tag => spec.version.to_s }

  spec.vendored_frameworks = "AFNetworkingHTTPClient.xcframework"

  spec.swift_versions = '5.0'
  spec.dependency 'AFNetworking', '4.0.1'

end