#
#  Be sure to run `pod spec lint AFNetworkingHTTPClient.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "AFNetworkingHTTPClient"
  spec.version      = "2.0.1"
  spec.summary      = "AFNetworkingHTTPClient SDK"

  spec.license          = { :type => 'Copyright' }
  spec.author           = 'Geopagos'
  spec.ios.deployment_target = '13.0'
  spec.homepage         = 'https://www.geopagos.com'
  spec.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-afnetworkinghttpclient.git', :tag => spec.version.to_s }

  spec.vendored_frameworks = "AFNetworkingHTTPClient.xcframework"

  spec.swift_versions = '5.0'
  spec.dependency 'AFNetworking', '4.0.1'

  # Skip this architecture to pass Pod validation since we removed the `arm64` simulator ARCH in order to use lipo later
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64', 'VALID_ARCHS' => 'x86_64 armv7 arm64' }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

end