Pod::Spec.new do |s|
  s.name             = 'GeoURLSessionHTTPClient'
  s.version          = '2.0.1'
  s.summary          = 'Allows perform http requests'

  s.homepage         = 'https://www.geopagos.com'
  s.license          = { :type => 'Copyright' }
  s.author           = 'Geopagos'
  s.source           = { :git => 'https://bitbucket.org/geopagos-sdk/ios-urlsessionhttpclient.git', :tag => s.version.to_s }

  s.ios.deployment_target = '13.0'
  s.vendored_frameworks = "URLSessionHTTPClient.xcframework"
  s.swift_versions = '5.0'
end